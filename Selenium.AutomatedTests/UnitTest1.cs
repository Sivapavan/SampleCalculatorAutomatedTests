﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZeusAutomationSuite.Helper;
using ZeusAutomationSuite.ApplicationPages;
using System.IO;
using System.Reflection;
using Microsoft.TeamFoundation.TestManagement.Client;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using ZeusAutomationSuite.DataProviders;

namespace SampleUnitTest
{
    [TestClass]
    public class SampleUnitTest
    {

        ZeusAutomationSuite.Helper.TestCaseHelper TestCase = null;
        ApplicationFeatures feature = new ApplicationFeatures();
        RunAutomation.Helpers.PopulateCOnfigurations Config = new RunAutomation.Helpers.PopulateCOnfigurations();
        CommonUtilities Utilities = new CommonUtilities();
        SQLDataProvider SQLProvider = new SQLDataProvider();
        static IWebDriver driver;
        public SampleUnitTest()
        {
            var chromedriverpath = Environment.GetEnvironmentVariable("chromedriverpath", EnvironmentVariableTarget.Machine);
           
            var url = Environment.GetEnvironmentVariable("TestURL", EnvironmentVariableTarget.Machine);
            if (driver == null)
            {
                
                driver = new ChromeDriver(chromedriverpath);
                driver.Navigate().GoToUrl(url);
                System.Threading.Thread.Sleep(10000);
            }
        }

        [TestInitialize]
        public void SetBrowserAsPerCOnfiguration()
        {
            if (Convert.ToString(TestContext.Properties["Module"]) == "TestData")
            {
                TestCase = new TestCaseHelper();
            }
            else
            {
                string ModuleName = Convert.ToString(TestContext.Properties["Module"]);
                TestCase = new TestCaseHelper(ModuleName);
            }

            try
            {
                TestContext.WriteLine(Environment.NewLine);
                TestContext.WriteLine("*************** Test Case Summary ***************");

                /*CHecking for MTM Configurations and Opening the browser accordingly*/
                if (TestContext.Properties["__Tfs_TestConfigurationName__"].ToString() != null)
                {

                    //BrowserWindow.CurrentBrowser = TestContext.Properties["__Tfs_TestConfigurationName__"].ToString();

                }

                if (TestContext.Properties["__Tfs_TestRunId__"].ToString() != null)
                {
                    int? testRunID = TestContext.Properties["__Tfs_TestRunId__"] as int?;
                    TestContext.WriteLine("Test Run ID: " + testRunID);
                    TestContext.WriteLine("Environment to Run the Test Cases: " + TFSHelper.GetTestRunTitle(testRunID));

                    //Setting the Environment to Run the test cases
                    ConfigHelper.Environment = TFSHelper.GetTestRunTitle(testRunID);
                    ConfigHelper.TestRunID = testRunID;

                    //Getting the Testing Tool from Configuration file
                    ConfigHelper.TestingTool = ConfigHelper.GetTestingTool();

                    /*Assign Role to Test User Accounts*/
                    //common.CreateUsers(testContextInstance);
                }
            }
            catch (Exception)
            {
                /*This code block will get executed while debugging/runing test locally or in case
                 Connection to TFS fails due any unknown reason*/
                ConfigHelper.TestingTool = "selenium";

                //common.CreateUsers(testContextInstance);
                Console.Error.WriteLine("We are here... in Catch box" + TestContext.TestName.ToString());
                ConfigHelper.TestRunID = -1;
            }
             
        }

        [TestCleanup]
        public void Cleanup()
        {
            //Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.Disabled;
        }       

        [TestCategory("Sanity")]
        [TestProperty("Module", "Enrollment")]
        [TestProperty("Role", "District Admin")]
        [TestProperty("BacklogItem", "WebTestforMobileAndPC")]
        [TestProperty("TFSID", "26209")]
        [Priority(1)]
        [TestMethod]
        public void AutomatedAddition()
        {
            
            Clear(driver);            
            //driver.Navigate().GoToUrl("http://localhost:1757/");
            IWebElement element = driver.FindElement(By.Id("b4"));
            element.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element4 = driver.FindElement(By.Id("add"));
            element4.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element3 = driver.FindElement(By.Id("b4"));
            element3.Click();
            //System.Threading.Thread.Sleep(200);
            IWebElement element1 = driver.FindElement(By.Id("eq"));
            element1.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element2 = driver.FindElement(By.Id("txt"));
            Assert.AreEqual(element2.GetAttribute("value"), "8");
            System.Threading.Thread.Sleep(800);
            //driver.Close();
        }

        [TestCategory("Sanity")]
        [TestProperty("Module", "Enrollment")]
        [TestProperty("Role", "District Admin")]
        [TestProperty("BacklogItem", "WebTestforMobileAndPC")]
        [TestProperty("TFSID", "26209")]
        [Priority(1)]
        [TestMethod]
        public void AutomatedSubtract()
        {
            //IWebDriver driver = new ChromeDriver(@"E:\exviewfiles\Selenium.AutomatedTests\Selenium.AutomatedTests\Drivers\");
            Clear(driver);            
            //driver.Navigate().GoToUrl("http://localhost:1757/");
            IWebElement element = driver.FindElement(By.Id("b4"));
            element.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element4 = driver.FindElement(By.Id("sub"));
            element4.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element3 = driver.FindElement(By.Id("b4"));
            element3.Click();
            System.Threading.Thread.Sleep(300);
            IWebElement element1 = driver.FindElement(By.Id("eq"));
            element1.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element2 = driver.FindElement(By.Id("txt"));
            Assert.AreEqual(element2.GetAttribute("value"), "0");
            System.Threading.Thread.Sleep(800);
            //driver.Close();
        }

        [TestCategory("Sanity")]
        [TestProperty("Module", "Enrollment")]
        [TestProperty("Role", "District Admin")]
        [TestProperty("BacklogItem", "WebTestforMobileAndPC")]
        [TestProperty("TFSID", "26209")]
        [Priority(1)]
        [TestMethod]
        public void AutomatedMultiply()
        {
            //IWebDriver driver = new ChromeDriver(@"E:\exviewfiles\Selenium.AutomatedTests\Selenium.AutomatedTests\Drivers\");
            Clear(driver);            
            //driver.Navigate().GoToUrl("http://localhost:1757/");
            IWebElement element = driver.FindElement(By.Id("b4"));
            element.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element4 = driver.FindElement(By.Id("multy"));
            element4.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element3 = driver.FindElement(By.Id("b4"));
            element3.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element1 = driver.FindElement(By.Id("eq"));
            element1.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element2 = driver.FindElement(By.Id("txt"));
            Assert.AreEqual(element2.GetAttribute("value"), "16");
            System.Threading.Thread.Sleep(800);
            //driver.Close();
        }

        [TestCategory("Sanity")]
        [TestProperty("Module", "Enrollment")]
        [TestProperty("Role", "District Admin")]
        [TestProperty("BacklogItem", "WebTestforMobileAndPC")]
        [TestProperty("TFSID", "26209")]
        [Priority(1)]
        [TestMethod]
        public void AutomatedDivision()
        {
            //IWebDriver driver = new ChromeDriver(@"E:\exviewfiles\Selenium.AutomatedTests\Selenium.AutomatedTests\Drivers\");
            Clear(driver);            
            
            IWebElement element = driver.FindElement(By.Id("b4"));
            element.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element4 = driver.FindElement(By.Id("div"));
            element4.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element3 = driver.FindElement(By.Id("b4"));
            element3.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element1 = driver.FindElement(By.Id("eq"));
            element1.Click();
            System.Threading.Thread.Sleep(200);
            IWebElement element2 = driver.FindElement(By.Id("txt"));
            Assert.AreEqual(element2.GetAttribute("value"), "1");
            System.Threading.Thread.Sleep(700);
           // driver.Close();
        }

        private void Clear(IWebDriver driver)
        {
            IWebElement element1 = driver.FindElement(By.Id("ce"));
            System.Threading.Thread.Sleep(300);           
            element1.Click();
            System.Threading.Thread.Sleep(300);
            IWebElement element2 = driver.FindElement(By.Id("ce"));
            element2.Click();
            System.Threading.Thread.Sleep(300);
            IWebElement element3 = driver.FindElement(By.Id("ce"));
            element3.Click();
        }

        

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

    }
}
